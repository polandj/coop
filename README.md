# ESP8266 Chicken Coop Controller #

Turn the $5 Witty Cloud Development (ESP8266-12E) board into a chicken coop controller.

![witty.jpg](https://bitbucket.org/repo/6LKdbj/images/808783627-witty.jpg)

### Features

 * Easy setup and control via WiFi
 * HTML5 Responsive Web Interface works on all devices
 * Programmable name for easy identification
 * Configurable timezone
 * Linear Actuator door control
 * LED light control

Checkout the [wiki](https://bitbucket.org/polandj/coop/wiki/Home) to get started!
