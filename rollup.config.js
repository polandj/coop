import svelte from 'rollup-plugin-svelte';
import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import uglify from 'rollup-plugin-uglify';

export default {
    input: 'www/js/main.svl',
    name: 'main',
    output: {
        file: 'devel/js/main.js',
        format: 'iife'
    },
    onwarn(warning) {
        // Ignore A11y warnings about a tags with no href
        if (warning.code === 'PLUGIN_WARNING' && warning.message.includes('href')) return;
    },
    plugins: [
        svelte({extensions: ['.svl']}),
        babel({
            exclude: 'node_modules/**',
            presets: [
                ["env", {modules: false, loose: true} ]
            ],
            plugins: ['external-helpers'],
            babelrc: false
        }),
        resolve({
            jsnext: true,
            main: true,
            browser: true,
        }),
        commonjs(),
        uglify()
    ]
}
