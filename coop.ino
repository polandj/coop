/* ESP8266 Chicken Coop Controller

   Controls linear actuator, lights, whatever

   Copyright(c) 2017 Jonathan Poland
*/
#include <EEPROM.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <sys/time.h>   // gettimeoday
#include <coredecls.h>  // settimeofday_cb
#include <TaskScheduler.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <Time.h>
#include <TimeLib.h>
#include <Timezone.h> //https://github.com/JChristensen/Timezone
#include <ArduinoOTA.h>

#include <Dusk2Dawn.h>

#include "FS.h"

#define INPUT_LDR A0

#define GPIO0 0
#define GPIO4 4
#define GPIO5 5 // Light
#define GPIO14 14 // Linear Actuator
#define GPIO16 16 // Linear Actuator

#define LED_TINY_BLU 2
#define INPUT_BUTTON 4
#define LED_GRN 12
#define LED_BLU 13
#define LED_RED 15

#define EEPROM_SIZE 512
#define DOOR_TRAVEL_TIME 90000 // Really only takes 15 seconds, but in cold weather...much slower
#define OPTIMAL_MINUTES_OF_LIGHT 840

#define CACHE_HEADER "max-age=259200"

#define INO_VERSION 8

// What we write to and from EEPROM
typedef struct cfg {
  int tz_offset;
  char name[20];
  int8_t door_mode;
  int8_t light_mode;
  int8_t door_open_offset;
  int8_t door_close_offset;
  float latitude;
  float longitude;
} Config;

// Globals
Ticker ticker;
WiFiUDP ntpUDP;
ESP8266WebServer server(80);
Scheduler runner;
WiFiManager wifiManager;
Config config;
bool web_reset = false;

// Timezone Globals
//US Eastern Time Zone (New York, Detroit)
TimeChangeRule usEDT PROGMEM = {"EDT", Second, Sun, Mar, 2, -240};  
TimeChangeRule usEST PROGMEM = {"EST", First, Sun, Nov, 2, -300};   
Timezone usET(usEDT, usEST) PROGMEM;

//US Central Time Zone (Chicago, Houston)
TimeChangeRule usCDT PROGMEM = {"CDT", Second, dowSunday, Mar, 2, -300};
TimeChangeRule usCST PROGMEM = {"CST", First, dowSunday, Nov, 2, -360};
Timezone usCT(usCDT, usCST) PROGMEM;

//US Mountain Time Zone (Denver, Salt Lake City)
TimeChangeRule usMDT PROGMEM = {"MDT", Second, dowSunday, Mar, 2, -360};
TimeChangeRule usMST PROGMEM = {"MST", First, dowSunday, Nov, 2, -420};
Timezone usMT(usMDT, usMST) PROGMEM;

//Arizona is US Mountain Time Zone but does not use DST
Timezone usAZ(usMST, usMST) PROGMEM;

//US Pacific Time Zone (Las Vegas, Los Angeles)
TimeChangeRule usPDT PROGMEM = {"PDT", Second, dowSunday, Mar, 2, -420};
TimeChangeRule usPST PROGMEM = {"PST", First, dowSunday, Nov, 2, -480};
Timezone usPT(usPDT, usPST) PROGMEM;

// Pointer to current TZ
TimeChangeRule genericRule = {"*", First, Sun, Mar, 2, 0};
Timezone genericTZ(genericRule, genericRule);
Timezone *tzp = &genericTZ;

String door_state = "unknown";
String light_state = "off";
int sunrise = 0;
int sunset = 0;

void door_stop_cb() {
  digitalWrite(GPIO16, HIGH);
  digitalWrite(GPIO14, HIGH);
  if (door_state == "opening") {
    door_state = "opened";
  } else if (door_state == "closing") {
    door_state = "closed";
  }
}

// Prototypes
void update_d2d_cb();
void automation_cb();

Task door_stop(DOOR_TRAVEL_TIME, TASK_ONCE, &door_stop_cb, &runner, false);
Task update_d2d(86400, TASK_ONCE, &update_d2d_cb, &runner, false);
Task automation(61, TASK_FOREVER, &automation_cb, &runner, true);

time_t last_ntp_sync = 0;
void ntp_sync_callback(void) {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  last_ntp_sync = tv.tv_sec;
}

time_t localTime(TimeChangeRule **tcr = NULL) {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  if (tcr) {
    return tzp->toLocal(tv.tv_sec, tcr);
  } else {
    return tzp->toLocal(tv.tv_sec);
  }
}

void update_d2d_cb() {
  TimeChangeRule *tcr = NULL;
  time_t tztime = localTime(&tcr);
  Dusk2Dawn localD2D(config.latitude, config.longitude, tcr->offset/60);
  sunrise = localD2D.sunrise(year(tztime), month(tztime), day(tztime), false);
  sunset = localD2D.sunset(year(tztime), month(tztime), day(tztime), false);
  int next_4am = 86400 - (tztime % 86400) + (3600 * 4);
  update_d2d.restartDelayed(next_4am * 1000);
}

void automation_cb() {
  time_t tztime = localTime();
  int now_min = hour(tztime) * 60 + minute(tztime);
  // Door Automation
  if (config.door_mode == 0) {
    int door_open_min = sunrise + config.door_open_offset;
    int door_close_min = sunset + config.door_close_offset;
    if (now_min >= door_open_min && now_min <= door_close_min) {
      if (!door_state.startsWith("open")) {
        open_door();
      }
    } else {
      if (!door_state.startsWith("clos")) {
        close_door();
      }
    }
  }
  // Light Automation
  if (config.light_mode == 0) {
    int augment_minutes = OPTIMAL_MINUTES_OF_LIGHT - (sunset - sunrise);
    if (augment_minutes > 0) {
      int light_on_min = sunrise - augment_minutes;
      int light_off_min = sunrise; 
      if (now_min >= light_on_min && now_min <= light_off_min) {
       if (light_state != "on") {
          light_on();
        }
      } else {
       if (light_state != "off") {
         light_off();
       }
      }
    } else {
      if (light_state != "off") {
        light_off();
      }
    }
  } 
}

// Used during setup to blink the LED
void tick() {
  static int color = LED_BLU;
  static int brightness = 0;
  if (brightness == 0) {
    brightness = map(analogRead(INPUT_LDR), 0, 1024, 1, 255);
  } else {
    brightness = 0;
    if (color == LED_BLU) {
      color = LED_RED;
    } else if (color == LED_RED) {
      color = LED_GRN;
    } else if (color == LED_GRN) {
      color = LED_BLU;  
    }
  }
  if (color == LED_BLU) {
    analogWrite(LED_GRN, 0);
    analogWrite(LED_BLU, brightness);
  } else if (color == LED_RED) {
    analogWrite(LED_BLU, 0);
    analogWrite(LED_RED, brightness);  
  } else if (color == LED_GRN) {
    analogWrite(LED_RED, 0);
    analogWrite(LED_GRN, brightness);  
  }
}

// Be a Wifi access point if we don't have any wifi config
void configModeCallback (WiFiManager *wifiManagerPtr) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(wifiManagerPtr->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.1, tick);
}

void update_internal_tz_offset(int offset) {
  if (offset == -14401) {
    tzp = &usET;
  } else if (offset == -18001) {
    tzp = &usCT;
  } else if (offset == -21601) {
    tzp = &usMT;
  } else if (offset == -25201) {
    tzp = &usPT;
  } else {
    genericRule.offset = offset/60;
    genericTZ.setRules(genericRule, genericRule);
    tzp = &genericTZ;
  }
  configTime(0, 0, "pool.ntp.org");
  update_d2d_cb();
}

// Set local clock
void time_init() {
  settimeofday_cb(ntp_sync_callback);
  update_internal_tz_offset(config.tz_offset);
  Serial.print("Attempting to synchronize time..");
  while (!last_ntp_sync) {
    delay(1000);
    Serial.print(".");
  }
  time_t tztime = localTime();
  Serial.printf("%02d:%02d", hour(tztime), minute(tztime));
  Serial.println();
}

// Set up the network
void wifi_init() {
  String apname = String(strlen(config.name) ? config.name: "coop-" + String(ESP.getChipId())) + "-AP";
  if (WiFi.SSID() != "") {
    wifiManager.setConnectTimeout(600);
    wifiManager.setConfigPortalTimeout(300);
  } else {
    wifiManager.setConfigPortalTimeout(0);
  }
  wifiManager.setAPCallback(configModeCallback);
  if (!wifiManager.autoConnect(apname.c_str())) {
    Serial.println("failed to connect and hit timeout");
    ESP.restart();
  }
}

void mdns_init() {
  const char * myname = strlen(config.name) ? config.name: "coop";
  if (!MDNS.begin(myname)) {
    Serial.println("Error setting my mDNS name");
  } else {
    Serial.println("mDNS started with name " + String(myname));
  }
  MDNS.addService("http", "tcp", 80);
}

void ota_init() {
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
    ESP.restart();
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("OTA updating enabled");
}

void config_default() {
  memset(&config, 0, sizeof(config));
  config.tz_offset = -14401; // Default to EST
}

// Load settings from EEPROM
void config_load() {
  uint32_t chipid;
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.get(0, chipid);
  if (chipid == ESP.getChipId()) {
    EEPROM.get(0 + sizeof(chipid), config);
    Serial.println("Loading saved configuration from EEPROM");
  } else {
    config_default();
  }
  EEPROM.end();
}

// Save settings to EEPROM
void config_save() {
  uint32_t chipid = ESP.getChipId();
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.put(0, chipid);
  EEPROM.put(0 + sizeof(chipid), config);
  EEPROM.commit();
  EEPROM.end();
}

String zeropad(uint8_t value) {
  String retval = String(value);
  if (retval.length() == 1) {
    retval = "0" + retval;
  }
  return retval;
}

// Send all config as JSON
void sendAll()
{
  String json = "{";
  char buf[10];
  time_t tztime = localTime();
  int augment_minutes = OPTIMAL_MINUTES_OF_LIGHT - (sunset - sunrise);
  
  // Config
  json += "\"tz_offset\": \"" + String(config.tz_offset) + "\",";
  json += "\"name\": \"" + String(config.name) + "\",";
  json += "\"door_mode\": \"" + String(config.door_mode) + "\",";
  json += "\"door_open_offset\": \"" + String(config.door_open_offset) + "\",";
  json += "\"door_close_offset\": \"" + String(config.door_close_offset) + "\",";
  json += "\"light_mode\": \"" + String(config.light_mode) + "\",";
  json += "\"latitude\": \"" + String(config.latitude) + "\",";
  json += "\"longitude\": \"" + String(config.longitude) + "\",";

  // State
  json += "\"hour\": \"" + zeropad(hour(tztime)) + "\",";
  json += "\"minute\": \"" + zeropad(minute(tztime)) + "\",";
  json += "\"door_state\": \"" + door_state + "\",";
  json += "\"light_state\": \"" + light_state + "\",";
  json += "\"brightness\": \"" + String(analogRead(INPUT_LDR)) + "\",";
  json += "\"bversion\": \"" + String(INO_VERSION) + "\",";

  if (config.light_mode == 0 && augment_minutes > 0) {
    int light_on_min = sunrise - augment_minutes;
    char lighton[] = "00:00";
    Dusk2Dawn::min2str(lighton, light_on_min);
    json += "\"light_on_at\": \"" + String(lighton) + "\",";
  }

  if (config.door_mode == 0) {
    int door_open_min = sunrise + config.door_open_offset;
    int door_close_min = sunset + config.door_close_offset;
    char door_open[] = "00:00";
    char door_close[] = "00:00";
    Dusk2Dawn::min2str(door_open, door_open_min);
    Dusk2Dawn::min2str(door_close, door_close_min);
    json += "\"door_open_at\": \"" + String(door_open) + "\",";
    json += "\"door_close_at\": \"" + String(door_close) + "\",";
  }

  char sunup[] = "00:00";
  char sundn[] = "00:00";
  Dusk2Dawn::min2str(sunup, sunrise);
  Dusk2Dawn::min2str(sundn, sunset);
  json += "\"sunrise\": \"" + String(sunup) + "\",";
  json += "\"sunset\": \"" + String(sundn) + "\"";

  json += "}";
  server.send(200, "text/json", json);
}

void httpError(String error) {
  server.send(400, "text/json", "{\"error\": \"" + error + "\"}");
}

String saveName(String newName)
{
  if (newName.length() >= sizeof(config.name) - 1) {
    return "Name is too long";
  }
  if (newName != String(config.name)) {
    strncpy(config.name, newName.c_str(), sizeof(config.name));
    MDNS.setInstanceName(newName);
  }
  return "";
}

void open_door() {
  digitalWrite(GPIO16, HIGH);
  digitalWrite(GPIO14, LOW);
  door_state = "opening";
  door_stop.restartDelayed(DOOR_TRAVEL_TIME);
}

void close_door() {
  digitalWrite(GPIO16, LOW);
  digitalWrite(GPIO14, HIGH);
  door_state = "closing";
  door_stop.restartDelayed(DOOR_TRAVEL_TIME);
}

void stop_door() {
  digitalWrite(GPIO16, HIGH);
  digitalWrite(GPIO14, HIGH);
  door_state = "stopped";
  door_stop.disable();
}

void light_on() {
  digitalWrite(GPIO5, LOW);
  light_state = "on";
}

void light_off() {
  digitalWrite(GPIO5, HIGH);
  light_state = "off";
}

void saveVars() {
  String json = "{";
  bool location_updated = false;
  for ( uint8_t i = 0; i < server.args(); i++ ) {
    String key = server.argName(i);
    String val = server.arg(i);
    
    if (key == "tz_offset") {
      config.tz_offset = val.toInt();
      update_internal_tz_offset(config.tz_offset);
      json += "\"tz_offset\": \"" + String(config.tz_offset) + "\",";
    }
    else if (key == "name") {
      String err = saveName(val);
      if (err.length()) {
        return httpError(err);
      }
      json += "\"name\": \"" + String(config.name) + "\",";
    }
    else if (key == "clear_config" && val == "on") {
      config_default();
      config_save();
    }
    else if (key == "reboot" && val == "on") {
      web_reset = true;
    }
    else if (key == "door_mode") {
      config.door_mode = val.toInt();
      json += "\"door_mode\": \"" + String(config.door_mode) + "\",";
    }
    else if (key == "door_open_offset") {
      config.door_open_offset = val.toInt();
      json += "\"door_open_offset\": \"" + String(config.door_open_offset) + "\",";
    } 
    else if (key == "door_close_offset") {
      config.door_close_offset = val.toInt();
      json += "\"door_close_offset\": \"" + String(config.door_close_offset) + "\",";
    } 
    else if (key == "door_state") {
      if (val == "open") {
        open_door();
      } 
      else if (val == "close") {
        close_door();
      } 
      else {
        stop_door();
      }
      json += "\"door_state\": \"" + door_state + "\",";
    }
    else if (key == "light_mode") {
      config.light_mode = val.toInt();
      json += "\"light_mode\": \"" + String(config.light_mode) + "\",";
    }
    else if (key == "light_state") {
      if (val == "on") {
        light_on();
      } 
      else if (val == "off") {
        light_off();
      }
      json += "\"light_state\": \"" + light_state + "\",";
    } else if (key == "latitude") {
        config.latitude = val.toFloat();
        location_updated = true;
        json += "\"latitude\": \"" + String(config.latitude) + "\",";
    } else if (key == "longitude") {
        config.longitude = val.toFloat();
        location_updated = true;
        json += "\"longitude\": \"" + String(config.longitude) + "\",";
    }
  }
  // Trim trailing comma
  if (json.endsWith(",")) {
    json.remove(json.length() - 1);
  }
  json += "}";
  config_save();
  if (location_updated) {
    update_d2d_cb();
  }
  server.send(200, "text/json", json);
}

// Setup handlers and start the web server
void webserver_init() {
  // Put these first so the / static doesn't override it
  server.on("/r/all", HTTP_GET, sendAll);
  server.on("/r/save", HTTP_POST, saveVars);
  server.serveStatic("/", SPIFFS, "/index.html", CACHE_HEADER);
  server.serveStatic("/", SPIFFS, "/", CACHE_HEADER);
  server.serveStatic("/fonts", SPIFFS, "/fonts", CACHE_HEADER);
  server.serveStatic("/js", SPIFFS, "/js", CACHE_HEADER);
  server.serveStatic("/css", SPIFFS, "/css", CACHE_HEADER);
  server.serveStatic("/images", SPIFFS, "/images", CACHE_HEADER);
  server.onNotFound([]() {
    // Count existing files, if any
    int filecount = 0;
    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {
      filecount++;
    }
    // Gather stats
    FSInfo fs_info;
    SPIFFS.info(fs_info);
    // Report findings
    String message = "<h1>File Not Found: " + server.uri() + "</h1>";
    if (fs_info.usedBytes == 0 || filecount == 0) {
      message += "<h2>It looks like you need to populate SPIFFS!</h2>";
    }
    message += "<p>SPIFFS Stats:</p>";
    message += "<ul>";
    message += "<li>Bytes used: " + String(fs_info.usedBytes) + " of " + String(fs_info.totalBytes) + "</li>";
    message += "<li>Number of files: " + String(filecount) + "</li>";
    message += "<li>MaxPathLen: " + String(fs_info.maxPathLength) + "</li>";
    message += "</ul>";
    message += "<p>Firmware version: " + String(INO_VERSION) + "</p>";
    server.send ( 404, "text/html", message );
  });
  server.begin();
  Serial.println("Web Server started");
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GRN, OUTPUT);
  pinMode(LED_BLU, OUTPUT);
  pinMode(LED_TINY_BLU, OUTPUT);

  pinMode(GPIO0, OUTPUT);
  pinMode(GPIO4, OUTPUT);
  pinMode(GPIO5, OUTPUT);
  pinMode(GPIO14, OUTPUT);
  pinMode(GPIO16, OUTPUT);
  
  // Turn off tiny blu light
  digitalWrite(LED_TINY_BLU, HIGH);
  stop_door();
  light_off();

  ticker.attach(0.5, tick);
  wifi_init();
  ticker.attach(0.5, tick);
  config_load();
  time_init();
  SPIFFS.begin();
  webserver_init();
  mdns_init();
  ota_init();
  update_d2d_cb();
  ticker.detach();

  // All is good: glow green LED
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_GRN, HIGH);
  digitalWrite(LED_BLU, LOW);

  ESP.wdtEnable(5000);

  runner.startNow();
  Serial.println("Startup complete!");
}

void wifi_check() {
  static int lastWifiReconnect = 0;
  static int lastWebServerRestart = 0;
  if (WiFi.status() != WL_CONNECTED) {
    int reconnectDuration = localTime() - lastWifiReconnect;
    if (reconnectDuration > 5) {
      Serial.println("Wifi disconnected, retrying");
      WiFi.begin();
      lastWifiReconnect = localTime();
    }
  } else if (WiFi.status() == WL_CONNECTED) {
    if (lastWifiReconnect > lastWebServerRestart) {
      Serial.println("Wifi reconnected, restarting webserver");
      server.close();
      server.begin();
      lastWebServerRestart = lastWifiReconnect;
    }
  }
}

void button_check() {
  static int lastButtonPressStart = 0;
  if (digitalRead(INPUT_BUTTON) == LOW) {
    // Button is pressed
    if (lastButtonPressStart == 0) {
      lastButtonPressStart = localTime();
    }
  } else if (lastButtonPressStart > 0) {
    // Button has been released
    int pressDuration = localTime() - lastButtonPressStart;
    if (pressDuration > 9) {
      Serial.println("Resetting CONFIG due to SUPER long button press");
      config_default();
      config_save();
    } else if (pressDuration > 4) {
      Serial.println("Resetting WIFI due to long button press");
      wifiManager.resetSettings();
    } else if (pressDuration > 1) {
      Serial.println("Resetting device due to short button press");
      ESP.restart();
    } 
    lastButtonPressStart = 0;
  }
}

void loop() {
  MDNS.update();
  server.handleClient();
  runner.execute();
  ArduinoOTA.handle();
  wifi_check();
  button_check();
  if (web_reset) {
    Serial.println("Resetting device due to web request");
    ESP.restart();
  }
}

