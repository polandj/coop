#
# Create the optimized resources to be uploaded 
# to the device byt outputing to the data directory.
# 
VERSION = 1.0
# Order matters for CSS and JS (can't just use find)
CSS = www/css/bulma.min.css www/css/fontello.css
# This cannot be gzipped
FILES = www/index.html
GENERATED = devel/css/styles.css devel/js/main.js
FAVICON = $(shell find www/favicon -type f -print)
FONTS = $(shell find www/font -type f -print)
IMAGES = $(shell find www/images -type f -print)

ALL = $(FAVICON:www/favicon/%=devel/%) $(FONTS:www/font%=devel/font%) $(IMAGES:www/images%=devel/images%) 

all: ${ALL} $(FILES:www/%=devel/%) ${GENERATED}
deploy: $(ALL:devel/%=data/%.gz) $(FILES:www/%=data/%) $(GENERATED:devel/%=data/%.gz)

watch: www/js/main.svl
	./node_modules/.bin/rollup -c --watch

devel/js/main.js: www/js/main.svl
	./node_modules/.bin/rollup -c 

devel/css/styles.css: ${CSS}
	@mkdir -p devel/css
	cat ${CSS} > $@

$(FAVICON:www/favicon/%=devel/%): ${FAVICON}
	@mkdir -p devel/
	cp $(@:devel/%=www/favicon/%) $@

$(FONTS:www/font%=devel/font%): ${FONTS}
	@mkdir -p devel/font
	cp $(@:devel/font%=www/font%) $@

$(IMAGES:www/images%=devel/images%): ${IMAGES}
	@mkdir -p devel/images
	cp $(@:devel/images%=www/images%) $@

$(FILES:www/%=devel/%): ${FILES}
	@mkdir -p devel
	cp $(@:devel/%=www/%) $@

$(FILES:www/%=data/%): $(FILES:www/%=devel/%)
	@mkdir -p data 
	cp $(@:data/%=devel/%) $@

$(ALL:devel/%=data/%.gz): ${ALL}
	@mkdir -p $(shell dirname $@)
	gzip -ck $(@:data/%.gz=devel/%) > $@ 

$(GENERATED:devel/%=data/%.gz): ${GENERATED}
	@mkdir -p $(shell dirname $@)
	sed -E 's/http:\/\/172\.27\.27\.[0-9]+//g' $(@:data/%.gz=devel/%)| gzip -cf > $@ 

clean:
	rm -rf devel/* data/*

